import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:movi2you/modules/shared/models/session_model.dart';
import 'package:movi2you/modules/shared/models/login_method_enum.dart';
import 'package:movi2you/modules/shared/models/token_model.dart';
import 'package:movi2you/modules/shared/repositories/app_repository.dart';

part 'app_controller.g.dart';

class AppController = AppControllerBase with _$AppController;

abstract class AppControllerBase with Store {
  final _appRepository = Modular.get<AppRepository>();

  AppControllerBase() {
    initialize();
  }

  LoginMethod? _loginMethod;
  Token? _token;
  Session? _session = Session();

  LoginMethod? get loginMethod => _loginMethod;
  Token? get token => _token;
  Session? get session => _session;

  Future<void> getMethodLogin() async {
    _loginMethod = await _appRepository.getMethodLogin();
  }

  Future<void> getToken() async {
    _token = await _appRepository.getToken();
  }

  Future<void> newGuestSession() async {
    _session = await _appRepository.newGuestSession();
  }

  Future<void> getGuestSessionId() async {
    _session!.guest_session_id = await _appRepository.getGuestSessionId();
  }

  Future<void> initialize() async {
    await getMethodLogin();
    await getToken();

    if (_loginMethod == LoginMethod.guest) {
      await getGuestSessionId();
      if (_session!.guest_session_id != null &&
          _session!.guest_session_id!.isNotEmpty) {
        Modular.to.pushReplacementNamed('/home/');
      } else {
        Modular.to.pushReplacementNamed('/login/');
      }
    } else if (_loginMethod == LoginMethod.emailAndPassword) {
      await getGuestSessionId();
      if (_session?.guest_session_id != null) {
        Modular.to.pushReplacementNamed('/home/');
      }
      Modular.to.pushReplacementNamed('/login/');
    } else {
      Modular.to.pushReplacementNamed('/login/');
    }
  }

  Future<void> logout() async {
    _session = null;
    _token = null;
    _loginMethod = LoginMethod.unlogged;
    _appRepository.logout();
    Modular.to.pushReplacementNamed('/login/');
  }

  @action
  Future<void> setSession(Session? value) async {
    _session = value;
  }
}
