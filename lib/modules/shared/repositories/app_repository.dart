import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:movi2you/modules/shared/models/session_model.dart';
import 'package:movi2you/modules/shared/models/login_method_enum.dart';
import 'package:movi2you/modules/shared/models/token_model.dart';
import 'package:movi2you/modules/shared/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppRepository {
  final _dio = Modular.get<Dio>();

  Future<LoginMethod> getMethodLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String status = prefs.getString('LoginMethod') ?? '';
    if (status == 'guest') {
      return LoginMethod.guest;
    } else if (status == 'emailAndPassword') {
      return LoginMethod.emailAndPassword;
    } else {
      return LoginMethod.unlogged;
    }
  }

  Future<Token?> getToken() async {
    Response response;
    try {
      response = await _dio.get(createRequestToken);
      if (response.statusCode == 200) {
        Token token = Token.fromJson(response.data as Map<String, dynamic>);
        return token;
      } else {
        return null;
      }
    } catch (_) {
      return null;
    }
  }

  Future<Session?> newGuestSession() async {
    Response response;
    try {
      response = await _dio.get(createGuestSession);
      if (response.statusCode == 200) {
        Session session =
            Session.fromJson(response.data as Map<String, dynamic>);
        SharedPreferences prefs = await SharedPreferences.getInstance();
        if (session.guest_session_id != null) {
          prefs.setString(
              'guestSessionId', session.guest_session_id.toString());
        }
        return session;
      } else {
        return null;
      }
    } catch (_) {
      return null;
    }
  }

  Future<void> setMethodLogin(LoginMethod loginMethod) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (loginMethod == LoginMethod.guest) {
      await prefs.setString('LoginMethod', 'guest');
    } else if (loginMethod == LoginMethod.emailAndPassword) {
      await prefs.setString('LoginMethod', 'emailAndPassword');
    } else {
      await prefs.setString('LoginMethod', 'unlogged');
    }
  }

  Future<String> getGuestSessionId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final result = prefs.getString('guestSessionId') ?? '';
    return result;
  }

  Future<void> logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.remove('LoginMethod');
    prefs.remove('guestSessionId');
  }
}
