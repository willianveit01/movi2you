import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:movi2you/modules/shared/controllers/app_controller.dart';
import 'package:movi2you/modules/shared/presentation/theme/theme.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Modular.get<AppController>();

    return const Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child: Text(
          'movi2you',
          style: CustomTextStyles.h1,
        ),
      ),
    );
  }
}
