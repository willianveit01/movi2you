import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:asuka/asuka.dart' as asuka;
import 'package:movi2you/modules/shared/presentation/theme/theme.dart';

class AppWidget extends StatelessWidget {
  const AppWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: themeApp,
      builder: asuka.builder,
      navigatorObservers: [
        asuka.asukaHeroController,
      ],
    ).modular();
  }
}
