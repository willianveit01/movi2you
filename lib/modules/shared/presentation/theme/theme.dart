import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomColors {
  static const Color primary = Color(0xFF000000);
  static const Color secundary = Color(0xFFFFFFFF);
  static const Color tertiary = Color(0xFF6C7378);
  static const Color quaternary = Color(0xFF1A1A1A);
}

class CustomTextStyles {
  static const TextStyle h1 = TextStyle(fontSize: 30);
  static const TextStyle h4 = TextStyle(fontSize: 18);
  static const TextStyle h5 = TextStyle(fontSize: 16);
  static const TextStyle title = TextStyle(
    fontSize: 22,
    fontWeight: FontWeight.bold,
  );
  static const TextStyle subtitle = TextStyle(
    fontSize: 18,
    color: CustomColors.tertiary,
  );
}

class CustomSpacing {
  static const double micro = 8;
  static const double xs = 12;
  static const double sm = 40;
}

ThemeData themeApp = ThemeData(
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ButtonStyle(
      backgroundColor: MaterialStateProperty.all(
        CustomColors.tertiary,
      ),
    ),
  ),
  scaffoldBackgroundColor: CustomColors.primary,
  textTheme: GoogleFonts.poppinsTextTheme().apply(
    bodyColor: CustomColors.secundary,
  ),
);
