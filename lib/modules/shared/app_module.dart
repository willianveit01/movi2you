import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:movi2you/modules/home/home_module.dart';
import 'package:movi2you/modules/login/login_module.dart';
import 'package:movi2you/modules/shared/repositories/app_repository.dart';
import 'controllers/app_controller.dart';
import 'presentation/pages/splash_page.dart';
import 'utils/constants.dart';

class AppModule extends Module {
  @override
  List<Bind> get binds => [
        Bind((i) => AppController()),
        Bind((i) => Dio(BaseOptions(
              baseUrl: urlBase,
              queryParameters: {
                'api_key': apiKey,
              },
            ))),
        Bind((i) => AppRepository()),
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute('/', child: (_, __) => const SplashPage()),
        ModuleRoute('/login', module: LoginModule()),
        ModuleRoute('/home', module: HomeModule()),
      ];
}
