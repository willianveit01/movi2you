const String urlBase = "https://api.themoviedb.org/3/";
const String apiKey = "af7025e193423e68a6dbbb2c9dea0c9a";
const String urlImages = 'https://image.tmdb.org/t/p/w200';
const String createGuestSession = 'authentication/guest_session/new';
const String createRequestToken = 'authentication/token/new';
