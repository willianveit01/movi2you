// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'token_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Token _$TokenFromJson(Map<String, dynamic> json) {
  return Token(
    success: json['success'] as bool?,
    expires_at: json['expires_at'] as String?,
    request_token: json['request_token'] as String?,
  );
}

Map<String, dynamic> _$TokenToJson(Token instance) => <String, dynamic>{
      'success': instance.success,
      'expires_at': instance.expires_at,
      'request_token': instance.request_token,
    };
