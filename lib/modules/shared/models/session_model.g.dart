// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'session_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Session _$SessionFromJson(Map<String, dynamic> json) {
  return Session(
    success: json['success'] as bool?,
    guest_session_id: json['guest_session_id'] as String?,
    expires_at: json['expires_at'] as String?,
  )..session_id = json['session_id'] as String?;
}

Map<String, dynamic> _$SessionToJson(Session instance) => <String, dynamic>{
      'success': instance.success,
      'session_id': instance.session_id,
      'guest_session_id': instance.guest_session_id,
      'expires_at': instance.expires_at,
    };
