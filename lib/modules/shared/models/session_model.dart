// ignore_for_file: non_constant_identifier_names

import 'package:json_annotation/json_annotation.dart';
part 'session_model.g.dart';

@JsonSerializable()
class Session {
  bool? success;
  String? session_id;
  String? guest_session_id;
  String? expires_at;

  Session({
    this.success,
    this.guest_session_id,
    this.expires_at,
  });

  factory Session.fromJson(Map<String, dynamic> json) =>
      _$SessionFromJson(json);
  Map<String, dynamic> toJson() => _$SessionToJson(this);
}
