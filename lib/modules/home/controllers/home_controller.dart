import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:movi2you/modules/home/models/movie_model.dart';
import 'package:movi2you/modules/home/repositories/home_repository.dart';
part 'home_controller.g.dart';

class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {
  final _homeRepository = Modular.get<HomeRepository>();

  _HomeControllerBase() {
    getHomePageMovies();
  }

  @action
  Future<void> getHomePageMovies() async {
    await getMostPopularMovies();
    await getMoviesInTheCinema();
    await getBestRatedMovies();
    await getComedyMovies();
    await getDramaMovies();
    await getScienceFictionMovies();
    await getActionMovies();
    await getWarMovies();
    await getWarMovies();
    await getRatedMovies();
  }

  @observable
  ObservableList<MovieModel> popularMovies = <MovieModel>[].asObservable();

  @observable
  ObservableList<MovieModel> moviesInTheCinema = <MovieModel>[].asObservable();

  @observable
  ObservableList<MovieModel> bestRatedMovies = <MovieModel>[].asObservable();

  @observable
  ObservableList<MovieModel> comedyMovies = <MovieModel>[].asObservable();

  @observable
  ObservableList<MovieModel> dramaMovies = <MovieModel>[].asObservable();

  @observable
  ObservableList<MovieModel> scienceFictionMovies =
      <MovieModel>[].asObservable();

  @observable
  ObservableList<MovieModel> actionMovies = <MovieModel>[].asObservable();

  @observable
  ObservableList<MovieModel> warMovies = <MovieModel>[].asObservable();

  @observable
  ObservableList<MovieModel> ratedMovies = <MovieModel>[].asObservable();

  @observable
  bool loadingPopularMovies = true;

  @observable
  bool loadingMoviesInTheCinema = true;

  @observable
  bool loadingbestRatedMovies = true;

  @observable
  bool loadingcomedyMovies = true;

  @observable
  bool loadingDramaMovies = true;

  @observable
  bool loadingScienceFictionMovies = true;

  @observable
  bool loadingActionMovies = true;

  @observable
  bool loadingWarMovies = true;

  @action
  Future<void> getMostPopularMovies() async {
    loadingPopularMovies = true;
    popularMovies.addAll(await _homeRepository.getMostPopularMovies());
    loadingPopularMovies = false;
  }

  @action
  Future<void> getMoviesInTheCinema() async {
    loadingMoviesInTheCinema = true;
    moviesInTheCinema.addAll(await _homeRepository.getMoviesInTheCinema());
    loadingMoviesInTheCinema = false;
  }

  @action
  Future<void> getBestRatedMovies() async {
    loadingbestRatedMovies = true;
    bestRatedMovies.addAll(await _homeRepository.getBestRatedMovies());
    loadingbestRatedMovies = false;
  }

  @action
  Future<void> getComedyMovies() async {
    loadingcomedyMovies = true;
    comedyMovies.addAll(await _homeRepository.getComedyMovies());
    loadingcomedyMovies = false;
  }

  @action
  Future<void> getDramaMovies() async {
    loadingDramaMovies = true;
    dramaMovies.addAll(await _homeRepository.getDramaMovies());
    loadingDramaMovies = false;
  }

  @action
  Future<void> getScienceFictionMovies() async {
    loadingScienceFictionMovies = true;
    scienceFictionMovies
        .addAll(await _homeRepository.getScienceFictionMovies());
    loadingScienceFictionMovies = false;
  }

  @action
  Future<void> getActionMovies() async {
    loadingActionMovies = true;
    actionMovies.addAll(await _homeRepository.getActionMovies());
    loadingActionMovies = false;
  }

  @action
  Future<void> getWarMovies() async {
    loadingWarMovies = true;
    warMovies.addAll(await _homeRepository.getWarMovies());
    loadingWarMovies = false;
  }

  @action
  Future<void> getRatedMovies() async {
    ratedMovies.clear();
    ratedMovies.addAll(await _homeRepository.getRatedMovies());
  }
}
