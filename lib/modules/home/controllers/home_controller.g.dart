// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeController on _HomeControllerBase, Store {
  final _$popularMoviesAtom = Atom(name: '_HomeControllerBase.popularMovies');

  @override
  ObservableList<MovieModel> get popularMovies {
    _$popularMoviesAtom.reportRead();
    return super.popularMovies;
  }

  @override
  set popularMovies(ObservableList<MovieModel> value) {
    _$popularMoviesAtom.reportWrite(value, super.popularMovies, () {
      super.popularMovies = value;
    });
  }

  final _$moviesInTheCinemaAtom =
      Atom(name: '_HomeControllerBase.moviesInTheCinema');

  @override
  ObservableList<MovieModel> get moviesInTheCinema {
    _$moviesInTheCinemaAtom.reportRead();
    return super.moviesInTheCinema;
  }

  @override
  set moviesInTheCinema(ObservableList<MovieModel> value) {
    _$moviesInTheCinemaAtom.reportWrite(value, super.moviesInTheCinema, () {
      super.moviesInTheCinema = value;
    });
  }

  final _$bestRatedMoviesAtom =
      Atom(name: '_HomeControllerBase.bestRatedMovies');

  @override
  ObservableList<MovieModel> get bestRatedMovies {
    _$bestRatedMoviesAtom.reportRead();
    return super.bestRatedMovies;
  }

  @override
  set bestRatedMovies(ObservableList<MovieModel> value) {
    _$bestRatedMoviesAtom.reportWrite(value, super.bestRatedMovies, () {
      super.bestRatedMovies = value;
    });
  }

  final _$comedyMoviesAtom = Atom(name: '_HomeControllerBase.comedyMovies');

  @override
  ObservableList<MovieModel> get comedyMovies {
    _$comedyMoviesAtom.reportRead();
    return super.comedyMovies;
  }

  @override
  set comedyMovies(ObservableList<MovieModel> value) {
    _$comedyMoviesAtom.reportWrite(value, super.comedyMovies, () {
      super.comedyMovies = value;
    });
  }

  final _$dramaMoviesAtom = Atom(name: '_HomeControllerBase.dramaMovies');

  @override
  ObservableList<MovieModel> get dramaMovies {
    _$dramaMoviesAtom.reportRead();
    return super.dramaMovies;
  }

  @override
  set dramaMovies(ObservableList<MovieModel> value) {
    _$dramaMoviesAtom.reportWrite(value, super.dramaMovies, () {
      super.dramaMovies = value;
    });
  }

  final _$scienceFictionMoviesAtom =
      Atom(name: '_HomeControllerBase.scienceFictionMovies');

  @override
  ObservableList<MovieModel> get scienceFictionMovies {
    _$scienceFictionMoviesAtom.reportRead();
    return super.scienceFictionMovies;
  }

  @override
  set scienceFictionMovies(ObservableList<MovieModel> value) {
    _$scienceFictionMoviesAtom.reportWrite(value, super.scienceFictionMovies,
        () {
      super.scienceFictionMovies = value;
    });
  }

  final _$actionMoviesAtom = Atom(name: '_HomeControllerBase.actionMovies');

  @override
  ObservableList<MovieModel> get actionMovies {
    _$actionMoviesAtom.reportRead();
    return super.actionMovies;
  }

  @override
  set actionMovies(ObservableList<MovieModel> value) {
    _$actionMoviesAtom.reportWrite(value, super.actionMovies, () {
      super.actionMovies = value;
    });
  }

  final _$warMoviesAtom = Atom(name: '_HomeControllerBase.warMovies');

  @override
  ObservableList<MovieModel> get warMovies {
    _$warMoviesAtom.reportRead();
    return super.warMovies;
  }

  @override
  set warMovies(ObservableList<MovieModel> value) {
    _$warMoviesAtom.reportWrite(value, super.warMovies, () {
      super.warMovies = value;
    });
  }

  final _$ratedMoviesAtom = Atom(name: '_HomeControllerBase.ratedMovies');

  @override
  ObservableList<MovieModel> get ratedMovies {
    _$ratedMoviesAtom.reportRead();
    return super.ratedMovies;
  }

  @override
  set ratedMovies(ObservableList<MovieModel> value) {
    _$ratedMoviesAtom.reportWrite(value, super.ratedMovies, () {
      super.ratedMovies = value;
    });
  }

  final _$loadingPopularMoviesAtom =
      Atom(name: '_HomeControllerBase.loadingPopularMovies');

  @override
  bool get loadingPopularMovies {
    _$loadingPopularMoviesAtom.reportRead();
    return super.loadingPopularMovies;
  }

  @override
  set loadingPopularMovies(bool value) {
    _$loadingPopularMoviesAtom.reportWrite(value, super.loadingPopularMovies,
        () {
      super.loadingPopularMovies = value;
    });
  }

  final _$loadingMoviesInTheCinemaAtom =
      Atom(name: '_HomeControllerBase.loadingMoviesInTheCinema');

  @override
  bool get loadingMoviesInTheCinema {
    _$loadingMoviesInTheCinemaAtom.reportRead();
    return super.loadingMoviesInTheCinema;
  }

  @override
  set loadingMoviesInTheCinema(bool value) {
    _$loadingMoviesInTheCinemaAtom
        .reportWrite(value, super.loadingMoviesInTheCinema, () {
      super.loadingMoviesInTheCinema = value;
    });
  }

  final _$loadingbestRatedMoviesAtom =
      Atom(name: '_HomeControllerBase.loadingbestRatedMovies');

  @override
  bool get loadingbestRatedMovies {
    _$loadingbestRatedMoviesAtom.reportRead();
    return super.loadingbestRatedMovies;
  }

  @override
  set loadingbestRatedMovies(bool value) {
    _$loadingbestRatedMoviesAtom
        .reportWrite(value, super.loadingbestRatedMovies, () {
      super.loadingbestRatedMovies = value;
    });
  }

  final _$loadingcomedyMoviesAtom =
      Atom(name: '_HomeControllerBase.loadingcomedyMovies');

  @override
  bool get loadingcomedyMovies {
    _$loadingcomedyMoviesAtom.reportRead();
    return super.loadingcomedyMovies;
  }

  @override
  set loadingcomedyMovies(bool value) {
    _$loadingcomedyMoviesAtom.reportWrite(value, super.loadingcomedyMovies, () {
      super.loadingcomedyMovies = value;
    });
  }

  final _$loadingDramaMoviesAtom =
      Atom(name: '_HomeControllerBase.loadingDramaMovies');

  @override
  bool get loadingDramaMovies {
    _$loadingDramaMoviesAtom.reportRead();
    return super.loadingDramaMovies;
  }

  @override
  set loadingDramaMovies(bool value) {
    _$loadingDramaMoviesAtom.reportWrite(value, super.loadingDramaMovies, () {
      super.loadingDramaMovies = value;
    });
  }

  final _$loadingScienceFictionMoviesAtom =
      Atom(name: '_HomeControllerBase.loadingScienceFictionMovies');

  @override
  bool get loadingScienceFictionMovies {
    _$loadingScienceFictionMoviesAtom.reportRead();
    return super.loadingScienceFictionMovies;
  }

  @override
  set loadingScienceFictionMovies(bool value) {
    _$loadingScienceFictionMoviesAtom
        .reportWrite(value, super.loadingScienceFictionMovies, () {
      super.loadingScienceFictionMovies = value;
    });
  }

  final _$loadingActionMoviesAtom =
      Atom(name: '_HomeControllerBase.loadingActionMovies');

  @override
  bool get loadingActionMovies {
    _$loadingActionMoviesAtom.reportRead();
    return super.loadingActionMovies;
  }

  @override
  set loadingActionMovies(bool value) {
    _$loadingActionMoviesAtom.reportWrite(value, super.loadingActionMovies, () {
      super.loadingActionMovies = value;
    });
  }

  final _$loadingWarMoviesAtom =
      Atom(name: '_HomeControllerBase.loadingWarMovies');

  @override
  bool get loadingWarMovies {
    _$loadingWarMoviesAtom.reportRead();
    return super.loadingWarMovies;
  }

  @override
  set loadingWarMovies(bool value) {
    _$loadingWarMoviesAtom.reportWrite(value, super.loadingWarMovies, () {
      super.loadingWarMovies = value;
    });
  }

  final _$getHomePageMoviesAsyncAction =
      AsyncAction('_HomeControllerBase.getHomePageMovies');

  @override
  Future<void> getHomePageMovies() {
    return _$getHomePageMoviesAsyncAction.run(() => super.getHomePageMovies());
  }

  final _$getMostPopularMoviesAsyncAction =
      AsyncAction('_HomeControllerBase.getMostPopularMovies');

  @override
  Future<void> getMostPopularMovies() {
    return _$getMostPopularMoviesAsyncAction
        .run(() => super.getMostPopularMovies());
  }

  final _$getMoviesInTheCinemaAsyncAction =
      AsyncAction('_HomeControllerBase.getMoviesInTheCinema');

  @override
  Future<void> getMoviesInTheCinema() {
    return _$getMoviesInTheCinemaAsyncAction
        .run(() => super.getMoviesInTheCinema());
  }

  final _$getBestRatedMoviesAsyncAction =
      AsyncAction('_HomeControllerBase.getBestRatedMovies');

  @override
  Future<void> getBestRatedMovies() {
    return _$getBestRatedMoviesAsyncAction
        .run(() => super.getBestRatedMovies());
  }

  final _$getComedyMoviesAsyncAction =
      AsyncAction('_HomeControllerBase.getComedyMovies');

  @override
  Future<void> getComedyMovies() {
    return _$getComedyMoviesAsyncAction.run(() => super.getComedyMovies());
  }

  final _$getDramaMoviesAsyncAction =
      AsyncAction('_HomeControllerBase.getDramaMovies');

  @override
  Future<void> getDramaMovies() {
    return _$getDramaMoviesAsyncAction.run(() => super.getDramaMovies());
  }

  final _$getScienceFictionMoviesAsyncAction =
      AsyncAction('_HomeControllerBase.getScienceFictionMovies');

  @override
  Future<void> getScienceFictionMovies() {
    return _$getScienceFictionMoviesAsyncAction
        .run(() => super.getScienceFictionMovies());
  }

  final _$getActionMoviesAsyncAction =
      AsyncAction('_HomeControllerBase.getActionMovies');

  @override
  Future<void> getActionMovies() {
    return _$getActionMoviesAsyncAction.run(() => super.getActionMovies());
  }

  final _$getWarMoviesAsyncAction =
      AsyncAction('_HomeControllerBase.getWarMovies');

  @override
  Future<void> getWarMovies() {
    return _$getWarMoviesAsyncAction.run(() => super.getWarMovies());
  }

  final _$getRatedMoviesAsyncAction =
      AsyncAction('_HomeControllerBase.getRatedMovies');

  @override
  Future<void> getRatedMovies() {
    return _$getRatedMoviesAsyncAction.run(() => super.getRatedMovies());
  }

  @override
  String toString() {
    return '''
popularMovies: ${popularMovies},
moviesInTheCinema: ${moviesInTheCinema},
bestRatedMovies: ${bestRatedMovies},
comedyMovies: ${comedyMovies},
dramaMovies: ${dramaMovies},
scienceFictionMovies: ${scienceFictionMovies},
actionMovies: ${actionMovies},
warMovies: ${warMovies},
ratedMovies: ${ratedMovies},
loadingPopularMovies: ${loadingPopularMovies},
loadingMoviesInTheCinema: ${loadingMoviesInTheCinema},
loadingbestRatedMovies: ${loadingbestRatedMovies},
loadingcomedyMovies: ${loadingcomedyMovies},
loadingDramaMovies: ${loadingDramaMovies},
loadingScienceFictionMovies: ${loadingScienceFictionMovies},
loadingActionMovies: ${loadingActionMovies},
loadingWarMovies: ${loadingWarMovies}
    ''';
  }
}
