// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$MovieController on _MovieControllerBase, Store {
  final _$isLikedAtom = Atom(name: '_MovieControllerBase.isLiked');

  @override
  bool get isLiked {
    _$isLikedAtom.reportRead();
    return super.isLiked;
  }

  @override
  set isLiked(bool value) {
    _$isLikedAtom.reportWrite(value, super.isLiked, () {
      super.isLiked = value;
    });
  }

  final _$loadingSimilarMoviesAtom =
      Atom(name: '_MovieControllerBase.loadingSimilarMovies');

  @override
  bool get loadingSimilarMovies {
    _$loadingSimilarMoviesAtom.reportRead();
    return super.loadingSimilarMovies;
  }

  @override
  set loadingSimilarMovies(bool value) {
    _$loadingSimilarMoviesAtom.reportWrite(value, super.loadingSimilarMovies,
        () {
      super.loadingSimilarMovies = value;
    });
  }

  final _$similarMoviesAtom = Atom(name: '_MovieControllerBase.similarMovies');

  @override
  ObservableList<MovieModel> get similarMovies {
    _$similarMoviesAtom.reportRead();
    return super.similarMovies;
  }

  @override
  set similarMovies(ObservableList<MovieModel> value) {
    _$similarMoviesAtom.reportWrite(value, super.similarMovies, () {
      super.similarMovies = value;
    });
  }

  final _$getSimilarMoviesAsyncAction =
      AsyncAction('_MovieControllerBase.getSimilarMovies');

  @override
  Future<void> getSimilarMovies(int movieId) {
    return _$getSimilarMoviesAsyncAction
        .run(() => super.getSimilarMovies(movieId));
  }

  @override
  String toString() {
    return '''
isLiked: ${isLiked},
loadingSimilarMovies: ${loadingSimilarMovies},
similarMovies: ${similarMovies}
    ''';
  }
}
