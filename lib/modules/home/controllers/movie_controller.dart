import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:movi2you/modules/home/controllers/home_controller.dart';
import 'package:movi2you/modules/home/models/movie_model.dart';
import 'package:movi2you/modules/home/repositories/home_repository.dart';

part 'movie_controller.g.dart';

class MovieController = _MovieControllerBase with _$MovieController;

abstract class _MovieControllerBase with Store {
  final _homeRepository = Modular.get<HomeRepository>();
  final _homeController = Modular.get<HomeController>();

  @observable
  bool isLiked = false;

  Future<void> isLikedMovie(MovieModel movie) async {
    for (var value in _homeController.ratedMovies) {
      if (movie.id == value.id) {
        isLiked = true;
      }
    }
  }

  Future<void> rate(int movieId) async {
    isLiked = true;
    await _homeRepository.rate(movieId);
    await _homeController.getRatedMovies();
  }

  Future<void> deleteRate(int movieId) async {
    isLiked = false;
    await _homeRepository.deleteRate(movieId);
    await _homeController.getRatedMovies();
  }

  @observable
  bool loadingSimilarMovies = true;

  @observable
  ObservableList<MovieModel> similarMovies = <MovieModel>[].asObservable();

  @action
  Future<void> getSimilarMovies(int movieId) async {
    loadingSimilarMovies = true;
    similarMovies.clear();
    similarMovies.addAll(await _homeRepository.getSimilarMovies(movieId));
    loadingSimilarMovies = false;
  }
}
