import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:movi2you/modules/home/controllers/home_controller.dart';
import 'package:movi2you/modules/home/presentation/widgets/movie_list.dart';
import 'package:movi2you/modules/shared/controllers/app_controller.dart';
import 'package:movi2you/modules/shared/presentation/theme/theme.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late AppController appController;
  late HomeController controller;

  @override
  void initState() {
    appController = Modular.get<AppController>();
    controller = Modular.get<HomeController>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: CustomColors.tertiary,
        onPressed: () => appController.logout(),
        child: const Icon(
          Icons.logout,
        ),
      ),
      body: CustomScrollView(
        slivers: [
          const _SliverAppBar(),
          _SliverSessionId(appController: appController),
          SliverFillRemaining(
            hasScrollBody: false,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Observer(builder: (_) {
                    return MovieList(
                      title: 'Mais populares',
                      movieList: controller.popularMovies,
                      isLoading: controller.loadingPopularMovies,
                    );
                  }),
                  Observer(builder: (_) {
                    return MovieList(
                      title: 'Nos cinemas',
                      movieList: controller.moviesInTheCinema,
                      isLoading: controller.loadingMoviesInTheCinema,
                    );
                  }),
                  Observer(builder: (_) {
                    return MovieList(
                      title: 'Melhores avaliações',
                      movieList: controller.bestRatedMovies,
                      isLoading: controller.loadingbestRatedMovies,
                    );
                  }),
                  Observer(builder: (_) {
                    return MovieList(
                      title: 'Melhores da comédia',
                      movieList: controller.comedyMovies,
                      isLoading: controller.loadingcomedyMovies,
                    );
                  }),
                  Observer(builder: (_) {
                    return MovieList(
                      title: 'Melhores do drama',
                      movieList: controller.dramaMovies,
                      isLoading: controller.loadingDramaMovies,
                    );
                  }),
                  Observer(builder: (_) {
                    return MovieList(
                      title: 'Melhores da Ficção Científica',
                      movieList: controller.scienceFictionMovies,
                      isLoading: controller.loadingScienceFictionMovies,
                    );
                  }),
                  Observer(builder: (_) {
                    return MovieList(
                      title: 'Melhores de ação',
                      movieList: controller.actionMovies,
                      isLoading: controller.loadingActionMovies,
                    );
                  }),
                  Observer(builder: (_) {
                    return MovieList(
                      title: 'Melhores de guerra',
                      movieList: controller.warMovies,
                      isLoading: controller.loadingWarMovies,
                    );
                  }),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _SliverSessionId extends StatelessWidget {
  const _SliverSessionId({
    Key? key,
    required this.appController,
  }) : super(key: key);

  final AppController appController;

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: SizedBox(
        height: 80,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            const Text('O id da sua sessão é: '),
            Text(appController.session?.guest_session_id ?? ''),
          ],
        ),
      ),
    );
  }
}

class _SliverAppBar extends StatelessWidget {
  const _SliverAppBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SliverAppBar(
      expandedHeight: 100.0,
      flexibleSpace: FlexibleSpaceBar(
        title: Text('movi2you', style: CustomTextStyles.h1),
      ),
      backgroundColor: CustomColors.primary,
    );
  }
}
