import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:movi2you/modules/home/controllers/movie_controller.dart';
import 'package:movi2you/modules/home/models/movie_model.dart';
import 'package:movi2you/modules/home/presentation/widgets/movie_list_item_widget.dart';
import 'package:movi2you/modules/shared/presentation/theme/theme.dart';
import 'package:movi2you/modules/shared/utils/constants.dart';

class MoviePage extends StatefulWidget {
  const MoviePage({Key? key, required this.movie}) : super(key: key);

  final MovieModel movie;

  @override
  State<MoviePage> createState() => _MoviePageState();
}

class _MoviePageState extends ModularState<MoviePage, MovieController> {
  @override
  Widget build(BuildContext context) {
    final heightScreen = MediaQuery.of(context).size.height;
    controller.isLikedMovie(widget.movie);
    controller.getSimilarMovies(widget.movie.id);

    return Scaffold(
      body: CustomScrollView(
        slivers: [
          _SliverAppBar(heightScreen: heightScreen, widget: widget),
          _SliverBoxTitleAndRated(widget: widget, controller: controller),
          _SliverBoxLikesAndRated(widget: widget, controller: controller),
          _SliverFillListMovies(controller: controller),
        ],
      ),
    );
  }
}

class _SliverFillListMovies extends StatelessWidget {
  const _SliverFillListMovies({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final MovieController controller;

  @override
  Widget build(BuildContext context) {
    return SliverFillRemaining(
      hasScrollBody: false,
      child: Observer(builder: (_) {
        if (controller.loadingSimilarMovies) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        return SingleChildScrollView(
          child: Column(
            children: [
              for (int i = 0; i < controller.similarMovies.length; i++)
                MovieListItemWidget(
                  movie: controller.similarMovies[i],
                ),
            ],
          ),
        );
      }),
    );
  }
}

class _SliverBoxLikesAndRated extends StatelessWidget {
  const _SliverBoxLikesAndRated({
    Key? key,
    required this.widget,
    required this.controller,
  }) : super(key: key);

  final MoviePage widget;
  final MovieController controller;

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Container(
        height: 40,
        alignment: AlignmentDirectional.topStart,
        padding: const EdgeInsets.symmetric(horizontal: CustomSpacing.xs),
        child: Row(
          children: [
            const Icon(
              Icons.favorite,
              color: CustomColors.tertiary,
              size: 18,
            ),
            const SizedBox(width: CustomSpacing.micro),
            Text(
              widget.movie.vote_count.toString() + ' Likes',
              style: CustomTextStyles.subtitle,
            ),
            const SizedBox(width: CustomSpacing.micro),
            const Icon(
              Icons.circle,
              color: CustomColors.tertiary,
              size: 18,
            ),
            const SizedBox(width: CustomSpacing.micro),
            Text(
              'Popularidade ' + widget.movie.popularity.toString(),
              style: CustomTextStyles.subtitle,
            ),
          ],
        ),
      ),
    );
  }
}

class _SliverBoxTitleAndRated extends StatelessWidget {
  const _SliverBoxTitleAndRated({
    Key? key,
    required this.widget,
    required this.controller,
  }) : super(key: key);

  final MoviePage widget;
  final MovieController controller;

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Container(
        height: 70,
        padding: const EdgeInsets.symmetric(horizontal: CustomSpacing.xs),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              child: Text(
                widget.movie.title ?? '',
                maxLines: 2,
                style: CustomTextStyles.title,
              ),
            ),
            const SizedBox(width: CustomSpacing.micro),
            Observer(builder: (_) {
              if (controller.isLiked) {
                return InkWell(
                    onTap: () => controller.deleteRate(widget.movie.id),
                    child: const Icon(
                      Icons.favorite,
                      color: CustomColors.secundary,
                    ));
              }
              return InkWell(
                  onTap: () => controller.rate(widget.movie.id),
                  child: const Icon(
                    Icons.favorite_border,
                    color: CustomColors.secundary,
                  ));
            }),
          ],
        ),
      ),
    );
  }
}

class _SliverAppBar extends StatelessWidget {
  const _SliverAppBar({
    Key? key,
    required this.heightScreen,
    required this.widget,
  }) : super(key: key);

  final double heightScreen;
  final MoviePage widget;

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      pinned: true,
      expandedHeight: heightScreen * .4,
      leading: Container(
        padding: const EdgeInsets.all(8.0),
        child: InkWell(
          onTap: () => Navigator.pop(context),
          child: const CircleAvatar(
            backgroundColor: CustomColors.quaternary,
            child: Icon(
              Icons.chevron_left_rounded,
            ),
          ),
        ),
      ),
      flexibleSpace: FlexibleSpaceBar(
        background: Column(
          children: [
            Container(
              height: heightScreen * .41,
              foregroundDecoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Colors.transparent,
                    Colors.transparent,
                    Colors.transparent,
                    Colors.black
                  ],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                ),
              ),
              child: CachedNetworkImage(
                imageUrl: '$urlImages${widget.movie.backdrop_path}',
                fit: BoxFit.cover,
              ),
            ),
          ],
        ),
      ),
      backgroundColor: CustomColors.primary,
    );
  }
}
