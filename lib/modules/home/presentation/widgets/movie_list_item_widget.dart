import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:movi2you/modules/home/models/movie_model.dart';
import 'package:movi2you/modules/shared/presentation/theme/theme.dart';
import 'package:movi2you/modules/shared/utils/constants.dart';

class MovieListItemWidget extends StatelessWidget {
  const MovieListItemWidget({Key? key, required this.movie}) : super(key: key);

  final MovieModel movie;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: CustomSpacing.micro),
      child: GestureDetector(
        onTap: () => Modular.to.pushNamed('movie', arguments: movie),
        child: Row(
          children: [
            CachedNetworkImage(
              imageUrl: '$urlImages${movie.poster_path}',
              height: 100,
              width: 70,
              fit: BoxFit.cover,
            ),
            const SizedBox(
              width: CustomSpacing.xs,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    movie.title ?? '',
                    style: CustomTextStyles.h4,
                  ),
                  Text('${getYear()}  ${getGenres()}')
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  String getYear() {
    return movie.release_date!.split('-')[0];
  }

  String getGenres() {
    return movie.genre_ids.toString();
  }
}
