import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:movi2you/modules/home/models/movie_model.dart';
import 'package:movi2you/modules/shared/utils/constants.dart';

class MovieWidget extends StatelessWidget {
  const MovieWidget({Key? key, required this.movie}) : super(key: key);

  final MovieModel movie;

  @override
  Widget build(BuildContext context) {
    if (movie.poster_path != null) {
      return InkWell(
        onTap: () {
          Modular.to.pushNamed('movie', arguments: movie);
        },
        child: ClipRRect(
          child: CachedNetworkImage(
            imageUrl: '$urlImages${movie.poster_path}',
            height: 100,
            width: 100,
            fit: BoxFit.cover,
          ),
          borderRadius: BorderRadius.circular(
            6,
          ),
        ),
      );
    } else {
      return InkWell(
        onTap: () {
          Modular.to.pushNamed('movie', arguments: movie);
        },
        child: Card(
          margin: EdgeInsets.zero,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              6,
            ),
          ),
          child: SizedBox(
            height: 100,
            width: 100,
            child: Center(
              child: Text(
                movie.title ?? '',
              ),
            ),
          ),
        ),
      );
    }
  }
}
