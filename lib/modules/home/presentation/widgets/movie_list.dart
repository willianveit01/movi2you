import 'package:flutter/material.dart';
import 'package:movi2you/modules/home/models/movie_model.dart';
import 'package:movi2you/modules/shared/presentation/theme/theme.dart';

import 'movie_widget.dart';

class MovieList extends StatelessWidget {
  const MovieList({
    Key? key,
    required this.movieList,
    required this.title,
    required this.isLoading,
  }) : super(key: key);

  final List<MovieModel> movieList;
  final String title;
  final bool isLoading;

  @override
  Widget build(BuildContext context) {
    final heightScreen = MediaQuery.of(context).size.height;
    final widthScreen = MediaQuery.of(context).size.width;
    return Column(
      children: [
        SizedBox(
          height: heightScreen * .04,
        ),
        Padding(
          padding: EdgeInsets.symmetric(
            horizontal: widthScreen * .04,
          ),
          child: Row(
            children: [
              Text(
                title,
              ),
              const Spacer(),
              const Icon(
                Icons.arrow_right_alt_rounded,
                color: CustomColors.tertiary,
              )
            ],
          ),
        ),
        SizedBox(
          height: heightScreen * .01,
        ),
        if (isLoading)
          const LinearProgressIndicator()
        else
          SizedBox(
            height: heightScreen * .2,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: movieList.length,
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.only(
                      left: index == 0 ? widthScreen * .04 : widthScreen * .02,
                      right: index == movieList.length - 1
                          ? widthScreen * .06
                          : widthScreen * .02,
                      top: heightScreen * .01,
                      bottom: heightScreen * .01,
                    ),
                    child: MovieWidget(
                      movie: movieList[index],
                    ),
                  );
                }),
          ),
      ],
    );
  }
}
