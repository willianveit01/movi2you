// ignore_for_file: non_constant_identifier_names

import 'package:json_annotation/json_annotation.dart';
part 'movie_model.g.dart';

@JsonSerializable()
class MovieModel {
  bool? adult;
  String? backdrop_path;
  dynamic belongs_to_collection;
  int? budget;
  dynamic genre_ids;
  String? homepage;
  int id;
  String? imdb_id;
  String? original_language;
  String? original_title;
  String? overview;
  double? popularity;
  String? poster_path;
  dynamic production_companies;
  dynamic production_countries;
  String? release_date;
  int? revenue;
  int? runtime;
  dynamic spoken_languages;
  String? status;
  String? tagline;
  String? title;
  bool? video;
  double? vote_average;
  int? vote_count;

  MovieModel({
    this.adult,
    this.backdrop_path,
    this.belongs_to_collection,
    this.budget,
    this.genre_ids,
    this.homepage,
    this.id = 0,
    this.imdb_id,
    this.original_language,
    this.original_title,
    this.overview,
    this.popularity,
    this.poster_path,
    this.production_companies,
    this.production_countries,
    this.release_date,
    this.revenue,
    this.runtime,
    this.spoken_languages,
    this.status,
    this.tagline,
    this.title,
    this.video,
    this.vote_average,
    this.vote_count,
  });

  factory MovieModel.fromJson(Map<String, dynamic> json) =>
      _$MovieModelFromJson(json);
  Map<String, dynamic> toJson() => _$MovieModelToJson(this);
}
