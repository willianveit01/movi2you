import 'package:flutter_modular/flutter_modular.dart';
import 'package:movi2you/modules/home/presentation/pages/movie_page.dart';
import 'package:movi2you/modules/home/repositories/home_repository.dart';
import 'controllers/home_controller.dart';
import 'controllers/movie_controller.dart';
import 'presentation/pages/home_page.dart';

class HomeModule extends Module {
  @override
  List<Bind> get binds => [
        Bind((i) => HomeController()),
        Bind.factory(((i) => MovieController())),
        Bind((i) => HomeRepository()),
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute('/', child: (_, __) => const HomePage()),
        ChildRoute('/movie', child: (_, args) => MoviePage(movie: args.data)),
      ];
}
