// ignore_for_file: unused_local_variable

import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:intl/intl.dart';
import 'package:movi2you/modules/home/models/movie_model.dart';
import 'package:movi2you/modules/shared/controllers/app_controller.dart';

class HomeRepository {
  Dio dio = Modular.get<Dio>();

  Future<List<MovieModel>> getMostPopularMovies() async {
    Response response;
    List<MovieModel> movies = [];

    try {
      response = await dio.get(
        'discover/movie?sort_by=popularity.desc&page=1',
      );
      if (response.statusCode == 200) {
        movies = (response.data['results'] as List).map((movie) {
          return MovieModel.fromJson(movie as Map<String, dynamic>);
        }).toList();
      }
    } catch (_) {
      // print(e);
    }
    return movies;
  }

  Future<List<MovieModel>> getMoviesInTheCinema() async {
    Response response;
    List<MovieModel> movies = [];

    String initialDate = DateFormat('yyyy-MM-dd')
        .format(DateTime.now().subtract(const Duration(days: 30)));
    String finalDate = DateFormat('yyyy-MM-dd').format(DateTime.now());

    try {
      response = await dio.get(
        '/discover/movie?primary_release_date.gte=$initialDate&primary_release_date.lte=$finalDate',
      );
      if (response.statusCode == 200) {
        movies = (response.data['results'] as List).map((movie) {
          return MovieModel.fromJson(movie as Map<String, dynamic>);
        }).toList();
      }
    } catch (_) {
      // print(e);
    }
    return movies;
  }

  Future<List<MovieModel>> getBestRatedMovies() async {
    Response response;
    List<MovieModel> movies = [];

    try {
      response = await dio.get(
        '/movie/top_rated',
      );
      if (response.statusCode == 200) {
        movies = (response.data['results'] as List).map((movie) {
          return MovieModel.fromJson(movie as Map<String, dynamic>);
        }).toList();
      }
    } catch (_) {
      // print(e);
    }
    return movies;
  }

  Future<List<MovieModel>> getComedyMovies() async {
    Response response;
    List<MovieModel> movies = [];

    try {
      response = await dio.get(
        '/discover/movie?with_genres=35',
      );
      if (response.statusCode == 200) {
        movies = (response.data['results'] as List).map((movie) {
          return MovieModel.fromJson(movie as Map<String, dynamic>);
        }).toList();
      }
    } catch (_) {
      // print(e);
    }
    return movies;
  }

  Future<List<MovieModel>> getDramaMovies() async {
    Response response;
    List<MovieModel> movies = [];

    try {
      response = await dio.get(
        '/discover/movie?with_genres=18',
      );
      if (response.statusCode == 200) {
        movies = (response.data['results'] as List).map((movie) {
          return MovieModel.fromJson(movie as Map<String, dynamic>);
        }).toList();
      }
    } catch (_) {
      // print(e);
    }
    return movies;
  }

  Future<List<MovieModel>> getScienceFictionMovies() async {
    Response response;
    List<MovieModel> movies = [];

    try {
      response = await dio.get(
        '/discover/movie?with_genres=878',
      );
      if (response.statusCode == 200) {
        movies = (response.data['results'] as List).map((movie) {
          return MovieModel.fromJson(movie as Map<String, dynamic>);
        }).toList();
      }
    } catch (_) {
      // print(e);
    }
    return movies;
  }

  Future<List<MovieModel>> getActionMovies() async {
    Response response;
    List<MovieModel> movies = [];

    try {
      response = await dio.get(
        '/discover/movie?with_genres=28',
      );
      if (response.statusCode == 200) {
        movies = (response.data['results'] as List).map((movie) {
          return MovieModel.fromJson(movie as Map<String, dynamic>);
        }).toList();
      }
    } catch (_) {
      // print(e);
    }
    return movies;
  }

  Future<List<MovieModel>> getWarMovies() async {
    Response response;
    List<MovieModel> movies = [];

    try {
      response = await dio.get(
        '/discover/movie?with_genres=10752',
      );
      if (response.statusCode == 200) {
        movies = (response.data['results'] as List).map((movie) {
          return MovieModel.fromJson(movie as Map<String, dynamic>);
        }).toList();
      }
    } catch (_) {
      // print(e);
    }
    return movies;
  }

  Future<List<MovieModel>> getRatedMovies() async {
    Response response;
    List<MovieModel> movies = [];

    final appController = Modular.get<AppController>();

    try {
      response = await dio.get(
        '/guest_session/${appController.session!.guest_session_id}/rated/movies?id=370172',
      );
      if (response.statusCode == 200) {
        movies = (response.data['results'] as List).map((movie) {
          return MovieModel.fromJson(movie as Map<String, dynamic>);
        }).toList();
      }
    } catch (_) {
      // print(e);
    }
    return movies;
  }

  Future<void> rate(int movieId) async {
    Response response;

    final appController = Modular.get<AppController>();

    try {
      response = await dio.post('/movie/$movieId/rating', data: {
        'value': 10,
      }, queryParameters: {
        'guest_session_id': appController.session!.guest_session_id,
      });
    } catch (_) {}
  }

  Future<void> deleteRate(int movieId) async {
    Response response;

    final appController = Modular.get<AppController>();

    try {
      response = await dio.delete('/movie/$movieId/rating', queryParameters: {
        'guest_session_id': appController.session!.guest_session_id,
      });
    } catch (_) {}
  }

  Future<List<MovieModel>> getSimilarMovies(int movieId) async {
    Response response;
    List<MovieModel> movies = [];

    try {
      response = await dio.get(
        '/movie/$movieId}/similar',
      );
      if (response.statusCode == 200) {
        movies = (response.data['results'] as List).map((movie) {
          return MovieModel.fromJson(movie as Map<String, dynamic>);
        }).toList();
      }
    } catch (_) {
      // print(e);
    }
    return movies;
  }
}
