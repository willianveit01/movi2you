import 'package:asuka/snackbars/asuka_snack_bar.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:movi2you/modules/shared/controllers/app_controller.dart';
import 'package:movi2you/modules/shared/models/login_method_enum.dart';
import 'package:movi2you/modules/shared/repositories/app_repository.dart';
part 'login_controller.g.dart';

class LoginController = LoginControllerBase with _$LoginController;

abstract class LoginControllerBase with Store {
  final _appRepository = Modular.get<AppRepository>();
  final _appController = Modular.get<AppController>();

  Future<void> signinAsGuest() async {
    if (_appController.token == null) {
      await _appController.getToken();
    }
    _appRepository.setMethodLogin(LoginMethod.guest);
    final guestSessionId = await _appRepository.newGuestSession();
    _appController.setSession(guestSessionId);
    if (guestSessionId != null) {
      Modular.to.pushReplacementNamed('/home/');
    } else {
      AsukaSnackbar.warning('Não foi possível logar-se como Conviado');
    }
  }
}
