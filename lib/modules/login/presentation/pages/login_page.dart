import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:movi2you/modules/login/controllers/login_controller.dart';
import 'package:movi2you/modules/shared/presentation/theme/theme.dart';

class LoginPage extends StatelessWidget {
  LoginPage({Key? key}) : super(key: key);

  final loginController = Modular.get<LoginController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: CustomSpacing.sm),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const Spacer(flex: 5),
            const Text(
              'movi2you',
              textAlign: TextAlign.center,
              style: CustomTextStyles.h1,
            ),
            const Spacer(flex: 3),
            // ElevatedButton(
            //   onPressed: () {},
            //   child: const Text(
            //     'Entrar com Email/Senha',
            //   ),
            // ),
            // const Spacer(),
            ElevatedButton(
              onPressed: () => loginController.signinAsGuest(),
              child: const Text(
                'Entrar como convidado',
              ),
            ),
            // const Spacer(),
            // ElevatedButton(
            //   onPressed: () {},
            //   child: const Text(
            //     'Criar conta grátis',
            //   ),
            // ),
            const Spacer(flex: 5),
          ],
        ),
      ),
    );
  }
}
