import 'package:flutter_modular/flutter_modular.dart';
import 'package:movi2you/modules/login/controllers/login_controller.dart';
import 'presentation/pages/login_page.dart';

class LoginModule extends Module {
  @override
  List<Bind> get binds => [
        Bind((i) => LoginController()),
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute('/', child: (_, __) => LoginPage()),
      ];
}
