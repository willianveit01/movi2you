Este é um simples aplicativo de catálogo de filmes.

Siga estes passos:

1 - Crie uma conta no themoviedb

https://www.themoviedb.org/signup

2 - Copie sua apiKey

3 - Cole sua apiKey no arquivo movi2you/lib/modules/shared/utils/constants.dart


# Obs: Alguns arquivos como controllers e models utilizam código automatizado.

- Para gerar código automatizado basta adicionar o @Annotacion adequado e executar o seguinte comando:

flutter packages pub run build_runner watch --delete-conflicting-outputs


## Informações úteis do app

Gerenciamento de estado: Mobx
Gerenciamento de rotas e injeção de dependências: Flutter_modular
Requisições http: Dio